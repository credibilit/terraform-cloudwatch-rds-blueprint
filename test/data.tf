data "aws_region" "current" {
  current = true
}

// Get the AZs from Amazon
data "aws_availability_zones" "azs" { }

/* AWS account
 * Declare this on TF_VAR_account environment variable with your AWS account id
 * for the test account
 */
variable account {}
