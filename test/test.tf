// VPC
module "test_vpc" {
  source  = "git::https://bitbucket.org/credibilit/terraform-vpc-blueprint.git?ref=0.0.8"
  account = "${var.account}"

  name                      = "cloudwatch_test_rds"
  domain_name               = "cloudwatch_test_rds.local"
  cidr_block                = "10.0.0.0/16"
  azs                       = "${data.aws_availability_zones.azs.names}"
  az_count                  = 4
  hosted_zone_comment       = "An internal hosted zone for testing"
  public_subnets_cidr_block = [
    "10.0.0.0/24",
    "10.0.1.0/24",
    "10.0.2.0/24",
    "10.0.3.0/24"
  ]
}

// RDS instance
module "test_rds" {
  source  = "git::ssh://git@bitbucket.org/credibilit/terraform-rds-mysql-blueprint.git?ref=0.0.7"
  account = "${var.account}"

  name                = "testcloudwatch"
  subnet_ids          = ["${module.test_vpc.public_subnets}"]
  vpc_id              = "${module.test_vpc.vpc}"
  password            = "123mudar"
  username            = "administrator"
  multi_az            = false
  skip_final_snapshot = true
}


// Action
resource "aws_sns_topic" "monitoring" {
  name         = "monitoring"
  display_name = "monitoring"
}

module "test_rds_cloudwatch_web" {
  source  = "../"
  account = "${var.account}"

  account_name  = "credibilit-tst"
  env           = "tst"
  instance_name = "${lookup(module.test_rds.db_instance, "id")}"
  alarm_actions = ["${aws_sns_topic.monitoring.arn}"]
}
