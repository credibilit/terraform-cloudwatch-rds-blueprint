# Cpu Utilization
resource "aws_cloudwatch_metric_alarm" "cpu_utilization" {
  alarm_name          = "[${var.account_name}] [rds] [${var.env}] ${var.instance_name} - CPU Utilization"
  comparison_operator = "${var.cpu_utilization_comparison_operator}"
  evaluation_periods  = "${var.cpu_utilization_evaluation_periods}"
  metric_name         = "CPUUtilization"
  namespace           = "AWS/RDS"
  period              = "${var.cpu_utilization_period}"
  statistic           = "${var.cpu_utilization_statistic}"
  threshold           = "${var.cpu_utilization_threshold}"
  unit                = "${var.cpu_utilization_unit}"
  alarm_actions       = ["${var.alarm_actions}"]
  dimensions {
    DBInstanceIdentifier = "${var.instance_name}"
  }
}
