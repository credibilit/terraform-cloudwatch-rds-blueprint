# Free Storage Space
resource "aws_cloudwatch_metric_alarm" "free_storage_space" {
  alarm_name          = "[${var.account_name}] [rds] [${var.env}] ${var.instance_name} - FreeStorageSpace"
  comparison_operator = "${var.free_storage_space_comparison_operator}"
  evaluation_periods  = "${var.free_storage_space_evaluation_periods}"
  metric_name         = "FreeStorageSpace"
  namespace           = "AWS/RDS"
  period              = "${var.free_storage_space_period}"
  statistic           = "${var.free_storage_space_statistic}"
  threshold           = "${var.free_storage_space_threshold}"
  unit                = "${var.free_storage_space_unit}"
  alarm_actions       = ["${var.alarm_actions}"]
  dimensions {
    DBInstanceIdentifier = "${var.instance_name}"
  }
}
