variable "account" {
  description = "The AWS account number"
}

variable "account_name" {
  description = "The AWS account name or alias"
}

variable "env" {
  description = "The environment name"
}

variable "instance_name" {
  description = "The AWS ec2 instance name"
}

variable "alarm_actions" {
  type = "list"
  description = "The AWS Cloud Watch alarm actions"
}

// CPU Utilization
variable "cpu_utilization_comparison_operator" {
  default = "GreaterThanOrEqualToThreshold"
}

variable "cpu_utilization_evaluation_periods" {
  default = 3
}

variable "cpu_utilization_period" {
  default = 300
}

variable "cpu_utilization_statistic" {
  default = "Average"
}

variable "cpu_utilization_threshold" {
  default = 90
}

variable "cpu_utilization_unit" {
  default = "Count"
}

// Free Storage Space
variable "free_storage_space_comparison_operator" {
  default = "LessThanOrEqualToThreshold"
}

variable "free_storage_space_evaluation_periods" {
  default = 3
}

variable "free_storage_space_period" {
  default = 300
}

variable "free_storage_space_statistic" {
  default = "Average"
}

variable "free_storage_space_threshold" {
  default = 10
}

variable "free_storage_space_unit" {
  default = "Percent"
}
